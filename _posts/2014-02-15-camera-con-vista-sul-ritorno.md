---
title: Camera con vista sul ritorno
slug: camera-con-vista-sul-ritorno
excerpt: "Guitar tablature for this nice italian song"
date: "2014-02-15T21:56:27.000Z"
author:
  name: Fabrizio Ruggeri
  picture: "/assets/blog/authors/fr.jpeg"
---

It's time to write the guitar tablature for this:

<a href="https://tabs.ultimate-guitar.com/tab/cappello-a-cilindro/camera-con-vista-sul-ritorno-chords-1463817" target="__blank">Guitar tab</a>
