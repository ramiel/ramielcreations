---
title: "A blog, just a blog!"
slug: a-blog-just-a-blog
excerpt: "I'm a web developer. With no time to develop a website of mine!"
date: "2013-07-06T16:58:33.000Z"
author:
  name: Fabrizio Ruggeri
  picture: "/assets/blog/authors/fr.jpeg"
---

I'm a web developer with no time to create my own website!

> But I definitely need one, so I'm letting WordPress handle it for me.

I should have known I'd end up working on it all night.  
Well, welcome everyone!

This blog is [opensource](https://gitlab.com/ramiel/ramielcreations)
