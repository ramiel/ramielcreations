---
title: The first draw of...
excerpt: "Me and @rafrart started a little old project..."
slug: it-starts-to-move
date: "2016-11-26T15:09:58.000Z"
author:
  name: Fabrizio Ruggeri
  picture: "/assets/blog/authors/fr.jpeg"
---

Me and [@rafrart](http://rafrart.com) started a little old project...

![Sketch dr Gregory Marlow](https://res.cloudinary.com/ramiel/image/upload/v1480176452/IMG_20161126_165001_syepix.jpg)
