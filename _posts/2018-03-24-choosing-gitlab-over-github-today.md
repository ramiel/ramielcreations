---
title: Choosing Gitlab over Github today
slug: choosing-gitlab-over-github-today
excerpt: Gitlab is a great platform but is it possible to use it for open source projects today?
date: "2018-03-24T19:48:31.000Z"
author:
  name: Fabrizio Ruggeri
  picture: "/assets/blog/authors/fr.jpeg"
---

Every developer out there used Github at least once to store their projects. Github imposed itself as code platform a couple of years ago when solutions as SourceForge saw its decline. Github helped all of us to have more confidence with git and also, in my opinion, created a bit of confusion deciding to name the code patch system "pull request".

Gitlab was born a bit later and it's a very similar platform which can be installed on your own servers.It's like having a Github service handled by you. Gitlab comes in several shapes and one is the community edition which is basically a free SaaS. This is the version I want to talk about today, the `gitlab.com` service.

The main reason why developers choose GitHub for their personal projects is because of the community. We're all there and the system works very well actually.

In the latest years, although, I started falling in love with Gitlab for some reasons I'm going to explain later in this article. I started using Gitlab for two reasons: it offers illimited private projects and it used to be adopted by some companies I worked with.

One month ago I started developing [Caravaggio](https://ramiel.gitlab.io/caravaggio/) a fast image web proxy written in Node.js. Because I need to keep the project as private at the beginning and because I'd want to use some tools which I like more on Gitlab, I hosted it there. It was the first time I used Gitlab for a personal project. Let me tell you what I learned and why I made this choice.

## What makes Gitlab a great tool

As many of you may have already noticed, often the code is the easier part of a project and, instead, you have to put more effort into other aspects. So, here my needs to release Caravaggio:

### Documentation

I need a way to give my users a good documentation which cannot be contained in the space of a readme file. Basically, I need a proper website. A statically generated website is enough and Gitlab provides `Gitlab-pages` which are more or less the same as `GitHub pages`. Gitlab-pages gives you a lot of freedom since you can choose any tool you like to produce your website and it's easy to integrate your building tool with Gitlab's CI system.

The result is this [beautiful documentation](https://ramiel.gitlab.io/caravaggio/) produced with [docusaurus](https://docusaurus.io/en/).

![Documentation website](/assets/blog/gitlab-over-github/documentation.png)

### Continuous integration and deployment

One of the key features which make me love Gitlab is the integrated continuous integration system. Github let you use an external system to run your CI tasks so you can choose great tools like Travis or CircleCI. The effort to integrate them is really low and there's nothing wrong with it.

Gitlab takes another approach: the CI system is totally integrated into the platform so you don't need to use an external service. This means you can watch your running pipelines from the same interface where you have your code and the integration is generally deeper. Then, they did something amazing: on gitlab.com you can use a series of shared runners, machines which runs your pipelines along with the ones from other projects. There is no limitation if your project is private, it has the same capability of the public projects (on Gitlab the concept of private repositories is not behind any pricing policy). If you want, and here is the magic, you can spawn runners of your own, on your local machine or on a server you have somewhere, and set them as dedicated runners for your project. This means you can choose to use shared or dedicated runners. Traditionally what companies do, when they need private resources to launch CI jobs, is to configure some Jenkins on their own servers. Gitlab solution is a lot better. You have nothing to configure (Jenkins can be hard sometimes), you just have to run a service (provided as OS package usually) and register it. You can start with the shared ones and, if you really need more power, spawn your owns and they're technically the same. Simple and powerful 😍.

For Caravaggio, my CI jobs are running test, checking code quality, deploying new versions on [npm](https://www.npmjs.com/package/caravaggio) and on the [docker store](https://store.docker.com/community/images/ramielcreations/caravaggio).

<!-- ![CI pipelines](/content/images/2018/03/Screenshot_20180324_152416.png) -->

Thanks to its integrated CI system, Gitlab is capable to provide an `auto-devops` feature which basically set for you a series of jobs based on your project type. If you do nothing you get, out of the box, tests, code quality checks and deployments on a kubernetes cluster 😱

### Drawbacks

Gitlab.com is an excellent solution but it could improve under some aspects. Mainly, I don't consider the platform reliable. Sometimes it stays unusable for hours because of a new release being deployed or other little problems: you can look at their twitter account [https://twitter.com/GitLabStatus](https://twitter.com/GitLabStatus)

This is something they should fix. Today, simply, is not acceptable that a service becomes unusable because of a release. This is a good reason, for companies, to install Gitlab on their server. Nonetheless, gitlab.com should try to improve a lot under this aspect.

[edit 2019] They are better now! It seems they get rid of any problem due to releases and the platform is far more reliable than before. Good job Gitlab!

### You will miss the community

This is true and this can be tough. Gitlab is an excellent choice when you're in an enterprise environment. You can install it on your servers and maintain it completely private and within the boundaries of your control.

Anytime I had to release something public instead, I chose GitHub because it's where people expect to find the code for open source projects. Every developer has an account on GitHub but maybe not one on Gitlab, so for them, it will be easier to contribute sending pull requests or opening issues.

It looks like I have no choice: need to go public, GitHub. Need to use the tools I love, go with Gitlab.

This time with Caravaggio I decided to make a bet and to leave it on Gitlab. I want to understand if the project will be less considered because of the platform. I want to understand if today, Gitlab is a good solution for public, open source projects, as it deserves to be.

I expect to have fewer interactions and probably some user will be discouraged when it will time to open an issue or send merge requests (the name of pull requests on Gitlab...a lot more meaningful!)
