import caravaggio from "caravaggio";
import nextPlugin from "caravaggio-plugin-nextjs";

const ONE_MONTH = 60 * 60 * 24 * 30;

export default caravaggio({
  logger: {
    options: {
      level: "error",
    },
  },
  basePath: "/api/assets",
  browserCache: {
    maxAge: ONE_MONTH,
  },
  plugins: {
    plugins: [
      {
        name: "nextjs",
        instance: nextPlugin(),
      },
    ],
  },
});
