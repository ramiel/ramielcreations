import { Center, Divider, Stack, Text } from "@mantine/core";
import { PostBody } from "../_components/Post/PostBody";
import { getPostBySlug } from "../_utils/api";
import { MainLayout } from "../_components/MainLayout";
import { PostCover } from "../_components/Post/PostCover";
import { PostComments } from "../_components/Post/PostComments";
import type { Metadata, ResolvingMetadata } from "next";
import { omit } from "radash";
import dayjs from "dayjs";
import { PostShare } from "../_components/Post/PostShare";

type PostPageProps = {
  params: Promise<{ slug: string }>;
};

export async function generateMetadata(props: PostPageProps, parent: ResolvingMetadata): Promise<Metadata> {
  const params = await props.params;
  const post = await getPostBySlug(params.slug, [
    "title",
    "excerpt",
    "date",
    "slug",
    "author",
    "ogImage",
    "cover",
    "tags",
  ]);

  if (!post) {
    return {};
  }

  const previousMeta = await parent;

  // optionally access and extend (rather than replace) parent metadata
  const previousImages = previousMeta.openGraph?.images || [];

  const postCover = post.ogImage || post.cover;
  const baseUrl = process.env.VERCEL
    ? process.env.VERCEL_ENV === "production"
      ? "https://ramielcreations.com"
      : `https://${process.env.VERCEL_URL}`
    : "http://localhost:3327";

  return {
    title: `Ramielcreations - ${post.title}`,
    description: post.excerpt || previousMeta.description,
    keywords: post.tags,
    authors: post.author?.name ? [{ name: post.author?.name }] : undefined,
    openGraph: {
      siteName: "Ramielcreations",
      title: post.title,
      description: post.excerpt || previousMeta.description || undefined,
      url: `${baseUrl}/${post.slug}`,
      tags: post.tags,
      type: "article",
      authors: post.author?.name ? [post.author?.name] : undefined,
      images: postCover
        ? [
            {
              ...omit(postCover, ["src"]),
              url: `${baseUrl}/api/assets/o:jpg/progressive:true/rs,s:1200x630,m:fill/overlay,url:${encodeURIComponent(
                `${baseUrl}/api/assets/o:png/progressive:true/rs,s:x250?image=%2Fimg%2Fbulb_on_small.webp`
              )},g:nw?image=${encodeURIComponent(postCover.src)}`,
            },
          ]
        : previousImages,
    },
  };
}

export default async function PostPage(props: PostPageProps) {
  const params = await props.params;
  const post = await getPostBySlug(params.slug, [
    "title",
    "date",
    "slug",
    "author",
    "ogImage",
    "cover",
    "draft",
    "content",
  ]);

  if (!post) {
    return <Text>Post not found</Text>;
  }

  return (
    <Stack>
      <MainLayout post={post}>
        <Center>
          <PostCover post={post} />
        </Center>
        <main>
          <PostBody post={post} />
          <PostShare slug={post.slug} />
          <Divider my="md" />
          <PostComments post={post} />
        </main>
      </MainLayout>
    </Stack>
  );
}
