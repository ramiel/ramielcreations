import { ColorSchemeScript, Container, MantineProvider } from "@mantine/core";
import type { Metadata, Viewport } from "next";
import { Roboto_Mono, Comfortaa } from "next/font/google";
import { Analytics } from "@vercel/analytics/react";
import "@mantine/core/styles.css";
import { theme } from "./_utils/theme";

export const metadata: Metadata = {
  title: "Ramielcreations",
  description: "My dev blog.. there's some music too ;)",
  openGraph: {
    title: "Ramielcreations",
    description: "My dev blog.. there's some music too ;)",
    url: "https://ramielcreations.com",
    siteName: "Ramielcreations",
    images: [
      {
        url: "https://ramielcreations.com/img/og_image.png", // Must be an absolute URL
        width: 800,
        height: 600,
        alt: "A light bulb",
      },
    ],
    locale: "en_US",
    type: "website",
  },
  icons: {
    icon: "/favicon/favicon-high.png",
    apple: "/favicon/favicon-high.png",
    other: {
      rel: "apple-touch-icon-precomposed",
      url: "/favicon/favicon-high.png",
    },
  },
};

export const viewport: Viewport = {
  themeColor: "#FCDB56",
};

const main = Comfortaa({
  subsets: ["latin"],
  variable: "--font-main",
  display: "swap",
  weight: ["400", "700"],
});

const mono = Roboto_Mono({
  subsets: ["latin"],
  variable: "--font-mono",
  display: "swap",
});

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html
      lang="en"
      className={`${main.variable} ${mono.variable}`}
      suppressHydrationWarning
    >
      <head>
        <ColorSchemeScript defaultColorScheme="auto" />
      </head>
      <body>
        <MantineProvider defaultColorScheme="auto" theme={theme}>
          <Container size="responsive">{children}</Container>
        </MantineProvider>
        <Analytics />
      </body>
    </html>
  );
}
