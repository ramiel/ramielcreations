"use client";

import { ReactNode } from "react";
import { useMantineTheme, Text, TextProps } from "@mantine/core";
/**
 * A text with a shadow on dark BG
 * @returns
 */
export function ShadowText({
  deg = 180,
  children,
  ...props
}: {
  deg?: number;
  children?: ReactNode;
} & Omit<TextProps, "variant" | "gradient">) {
  const theme = useMantineTheme();

  return (
    <>
      <Text
        variant="gradient"
        gradient={{ from: "#D19A05", to: theme.colors.yellow[0], deg }}
        lightHidden
        {...props}
      >
        {children}
      </Text>

      <Text {...props} darkHidden>
        {children}
      </Text>
    </>
  );
}
