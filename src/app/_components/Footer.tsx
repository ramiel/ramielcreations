import { Text, Group, Anchor, ActionIcon, Tooltip } from "@mantine/core";
import { LightSwitch } from "./LightSwitch";
import { IconBrandGitlab } from "@tabler/icons-react";

export function Footer() {
  return (
    <Group component="footer" mt={32} gap="xl" py="md" align="baseline">
      <Group align="baseline" gap="xs">
        <Text span size="sm">
          © {new Date().getFullYear()} All rights reserved
        </Text>
        <Tooltip label="This blog is opensource!">
          <Anchor
            href="https://gitlab.com/ramiel/ramielcreations"
            target="_blank"
            underline="never"
            style={{ alignSelf: "center" }}
          >
            <IconBrandGitlab size={20} />
          </Anchor>
        </Tooltip>
      </Group>
      <Text span size="sm">
        Logo by&nbsp;
        <Anchor href="https://www.instagram.com/rafrart/" target="__blank">
          RafrArt
        </Anchor>
      </Text>
      <Text span size="sm">
        Play{" "}
        <Anchor href="https://sudoku.ramielcreations.com" target="_blank">
          sudoku
        </Anchor>
      </Text>
      <LightSwitch />
    </Group>
  );
}
