import { Group, Anchor } from "@mantine/core";
import NextLink from "next/link";
import { LightSwitch } from "./LightSwitch";

export function NavMenu() {
  return (
    <Group gap="5rem" role="navigation">
      <Anchor href="/" component={NextLink} variant="text">
        Home
      </Anchor>
      <Anchor href="/about" component={NextLink}>
        About
      </Anchor>
      <LightSwitch />
    </Group>
  );
}
