"use client";

import NextImage from "next/image";
import { useColoSwitcher } from "../_hooks/useColorSwitcher";
import cx from "clsx";
import classes from "./SiteLogo.module.css";

export function SiteLogo({ width = 240 }: { width?: number }) {
  const { toggleColorScheme } = useColoSwitcher();
  const height = (371 / 240) * width;
  return (
    <>
      <NextImage
        src="/img/bulb_on_small.webp"
        width={width}
        height={height}
        priority
        alt="A power on lightbulb"
        onClick={toggleColorScheme}
        className={cx(classes.icon, classes.light)}
      />
      <NextImage
        src="/img/bulb_off_small.webp"
        width={width}
        height={height}
        priority
        alt="A power off lightbulb"
        onClick={toggleColorScheme}
        className={cx(classes.icon, classes.dark)}
      />
    </>
  );
}
