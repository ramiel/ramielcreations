"use client";

import { Switch, rem, useMantineTheme } from "@mantine/core";
import { IconMoonStars, IconSun } from "@tabler/icons-react";
import { useColoSwitcher } from "../_hooks/useColorSwitcher";
import { useEffect, useState } from "react";

export function LightSwitch() {
  const theme = useMantineTheme();
  const { toggleColorScheme, isDark } = useColoSwitcher();
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    setChecked(isDark);
  }, [isDark]);
  return (
    <Switch
      aria-label="Turn on or off dark mode"
      size="md"
      color="dark.4"
      checked={checked}
      onChange={toggleColorScheme}
      offLabel={
        <IconSun
          style={{ width: rem(16), height: rem(16) }}
          stroke={2.5}
          color={theme.colors.yellow[8]}
        />
      }
      onLabel={
        <IconMoonStars
          style={{ width: rem(16), height: rem(16) }}
          stroke={2.5}
          color={theme.colors.blue[6]}
        />
      }
    />
  );
}
