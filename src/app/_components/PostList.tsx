"use client";

import type Post from "@/types/post";
import { Anchor, Box, Group, Stack, Text, Title } from "@mantine/core";
import NextLink from "next/link";
import { toReadableDate } from "../_utils/util";
import { PostCover } from "./Post/PostCover";
import React from "react";

function PostCoverWithLink({
  post,
  withPriority,
}: {
  post: Pick<Post, "cover" | "title" | "slug">;
  withPriority?: boolean;
}) {
  return post.cover ? (
    <NextLink href={`/${post.slug}`}>
      <PostCover post={post} priority={withPriority} width={400} height={400} />
    </NextLink>
  ) : null;
}

export function PostList({
  posts,
}: {
  posts: Pick<
    Post,
    "title" | "date" | "slug" | "author" | "cover" | "excerpt" | "draft"
  >[];
}) {
  return (
    <Stack>
      {posts.map((post, i) => {
        const isEven = i % 2 === 0;
        const title = (
          <Anchor component={NextLink} href={`/${post.slug}`} underline="never">
            <Title order={2}>{post.title}</Title>
          </Anchor>
        );
        const excerpt = (
          <Stack maw="30rem" gap="xs">
            <Text tt="uppercase" c="dimmed" size="xs">
              {toReadableDate(post.date)}
            </Text>
            <Text>{post.excerpt}</Text>
          </Stack>
        );
        return (
          <React.Fragment key={`${post.slug}-${post.date}`}>
            {/* mobile */}
            <Stack align="flex-start" maw={400} mb={48} hiddenFrom="sm">
              {title}
              <PostCoverWithLink post={post} withPriority={i < 2} />
              {excerpt}
            </Stack>
            {/* desktop */}
            <Group wrap="nowrap" align="flex-start" visibleFrom="sm">
              {post.cover ? (
                <Box style={{ order: isEven ? 0 : 1 }}>
                  <PostCoverWithLink post={post} withPriority={i < 2} />
                </Box>
              ) : null}
              <Stack maw="30rem">
                {title}
                {excerpt}
              </Stack>
            </Group>
          </React.Fragment>
        );
      })}
    </Stack>
  );
}
