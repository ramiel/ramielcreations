"use client";
import type Post from "@/types/post";
import { CodeHighlight } from "@mantine/code-highlight";
import {
  Anchor,
  Blockquote,
  Box,
  Divider,
  Image,
  List,
  Text,
  Title,
} from "@mantine/core";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import rehypeSlug from "rehype-slug";
import classes from "./Post.module.css";
import NextLink from "next/link";
import "@mantine/code-highlight/styles.css";

export function PostBody({ post }: { post: Post }) {
  return post?.content ? (
    <Box maw={800} w="100vw" px="1rem">
      <ReactMarkdown
        rehypePlugins={[rehypeRaw, rehypeSlug]}
        components={{
          p: ({ node, ref, ...props }) => <Text py="1em" fw={400} {...props} />,
          a: ({ node, ref, href, ...props }) => (
            <Anchor
              component={
                (href as string).startsWith("/") ? NextLink : undefined
              }
              href={href}
              {...props}
            />
          ),
          divider: ({ node, id, children, ...props }) => (
            <>
              <Divider {...props} />
              {children}
            </>
          ),
          h1: ({ node, id, ...props }) => (
            <Anchor id={id} href={`#${id}`}>
              <Title order={1} {...props} />
            </Anchor>
          ),
          h2: ({ node, id, ...props }) => (
            <Anchor id={id} href={`#${id}`}>
              <Title order={2} {...props} />
            </Anchor>
          ),
          h3: ({ node, id, ...props }) => (
            <Anchor id={id} href={`#${id}`}>
              <Title order={3} {...props} />
            </Anchor>
          ),
          h4: ({ node, id, ...props }) => (
            <Anchor id={id} href={`#${id}`}>
              <Title order={4} {...props} />
            </Anchor>
          ),
          h5: ({ node, id, ...props }) => (
            <Anchor id={id} href={`#${id}`}>
              <Title order={5} {...props} />
            </Anchor>
          ),
          h6: ({ node, id, ...props }) => (
            <Anchor id={id} href={`#${id}`}>
              <Title order={6} {...props} />
            </Anchor>
          ),
          img: ({ node, alt, ref, ...props }) => (
            <Image
              {...props}
              alt={alt || "Missing alt for this image"}
              className={classes.image}
              bd="1px solid grey"
            />
          ),
          blockquote({ node, ref, ...props }) {
            return <Blockquote {...props} />;
          },
          code({ node, className, children, ref, ...props }) {
            const match = /language-(\w+)/.exec(className || "");
            const language = match?.[1];
            return language ? (
              <CodeHighlight
                code={String(children)}
                language={language}
                classNames={{
                  pre: classes.codeHighlight,
                }}
              />
            ) : (
              <Text fs="italic" fw={700} className={className} {...props} span>
                {children}
              </Text>
            );
          },
          ol: ({ node, ...props }) => <List {...props} type="ordered" />,
          ul: ({ node, ...props }) => <List {...props} />,
          li: ({ node, ...props }) => <List.Item {...props} />,
          strong: ({ node, ...props }) => (
            <Text component="strong" fw={700} {...props} />
          ),
          small: ({ node, ref, ...props }) => (
            <Text span size="xs" {...props} />
          ),
        }}
      >
        {post?.content}
      </ReactMarkdown>
    </Box>
  ) : null;
}
