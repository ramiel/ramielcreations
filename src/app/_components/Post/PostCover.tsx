import type Post from "@/types/post";
import NextImage from "next/image";
import classes from "./Post.module.css";
import { AspectRatio } from "@mantine/core";
import { useMemo } from "react";

function coverAdaptedSize(cover: Post["cover"], maxH: number) {
  let width = cover?.width || 400;
  let height = cover?.height || 400;
  const ratio = height / maxH;
  width = width / ratio;
  height = height / ratio;
  return { width, height };
}

export function PostCover({
  post,
  mah = 400,
  priority,
  width: suggestedWidth,
  height: suggestedHeight,
}: {
  post: Pick<Post, "cover" | "title">;
  priority?: boolean;
  mah?: number;
  width?: number;
  height?: number;
}) {
  const { width, height } = useMemo(() => {
    const goodSizes = coverAdaptedSize(post.cover, mah);
    return {
      width: suggestedWidth || goodSizes.width,
      height: suggestedHeight || goodSizes.height,
    };
  }, [mah, post.cover, suggestedHeight, suggestedWidth]);
  return post.cover ? (
    <AspectRatio ratio={width / height} maw={"100vw"}>
      <NextImage
        src={post.cover.src}
        width={width}
        height={height}
        alt={post.title}
        className={classes.image}
        priority={priority}
      />
    </AspectRatio>
  ) : null;
}
