"use client";
import type Post from "@/types/post";
import { Title } from "@mantine/core";
import { ShadowText } from "../ShadowText";

interface PostTitleProps {
  post: Post;
  /**
   * The direction of the shadow on dark bg.
   * Default 180
   */
  deg?: number;
}

export function PostTitle({ post, deg = 180 }: PostTitleProps) {
  return (
    <Title order={1}>
      <ShadowText hiddenFrom="sm" size="3rem" ta="center" deg={deg}>
        {post.title}
      </ShadowText>

      <ShadowText visibleFrom="sm" size="4rem" ta="center" deg={deg}>
        {post.title}
      </ShadowText>
    </Title>
  );
}
