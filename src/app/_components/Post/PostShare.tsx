import { Anchor, Group, Stack, Text } from "@mantine/core";
import {
  IconBrandFacebook,
  IconBrandLinkedin,
  IconBrandX,
} from "@tabler/icons-react";
import { getShareUrl, SocialPlatforms } from "@phntms/react-share";

export function PostShare({ slug }: { slug: string }) {
  const url = `https://ramielcreations.com/${slug}`;

  return (
    <Stack my="md">
      <Text>Share on:</Text>
      <Group>
        <Anchor
          href={getShareUrl(SocialPlatforms.Twitter, {
            url,
          })}
          target="_blank"
        >
          <IconBrandX />
        </Anchor>
        <Anchor
          href={getShareUrl(SocialPlatforms.Linkedin, {
            url,
          })}
          target="_blank"
        >
          <IconBrandLinkedin />
        </Anchor>
        <Anchor
          href={getShareUrl(SocialPlatforms.Facebook, {
            url,
          })}
          target="_blank"
        >
          <IconBrandFacebook />
        </Anchor>
      </Group>
    </Stack>
  );
}
