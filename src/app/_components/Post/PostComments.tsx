"use client";

import Post from "@/types/post";
import Script from "next/script";
import { useMemo } from "react";
import dayjs from "dayjs";

export function PostComments({ post }: { post: Post }) {
  const useSlugMethod = useMemo(() => {
    return dayjs(post.date).isAfter("2024-03-01");
  }, [post.date]);

  // useEffect(() => {
  //   return () => {
  //     document.getElementById(`gc-${post.slug}`)?.remove();
  //   };
  // }, []);

  return (
    <>
      <div id="graphcomment"></div>
      <Script id={`gc-${post.slug}`}>
        {`
       /* - - - CONFIGURATION VARIABLES - - - */
  
  var __semio__params = {
    graphcommentId: "ramielcreations", // make sure the id is yours
    
    behaviour: {
      // HIGHLY RECOMMENDED
      //  uid: "...", // uniq identifer for the comments thread on your page (ex: your page id)
      ${useSlugMethod ? `uid: "${post.slug}",` : ""}
    },
    
    // configure your variables here
    
  }

  /* - - - DON'T EDIT BELOW THIS LINE - - - */

  function __semio__onload() {
    __semio__gc_graphlogin(__semio__params)
  }


  (function() {
    var gc = document.createElement('script'); gc.type = 'text/javascript'; gc.async = true;
    gc.onload = __semio__onload; gc.defer = true; gc.src = 'https://integration.graphcomment.com/gc_graphlogin.js?' + Date.now();
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(gc);
  })();
      `}
      </Script>
    </>
  );
}
