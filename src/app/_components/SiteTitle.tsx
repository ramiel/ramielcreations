"use client";
import { Title } from "@mantine/core";
import { ShadowText } from "./ShadowText";

export function SiteTitle() {
  return (
    <Title order={1} size="4rem">
      <ShadowText size="4rem" ta="center">
        Ramiel&apos;s Creations
      </ShadowText>
    </Title>
  );
}
