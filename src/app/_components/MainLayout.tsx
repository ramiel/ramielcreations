import type Post from "@/types/post";
import { Stack, Center, Divider, Box, Group } from "@mantine/core";
import type { ReactNode } from "react";
import { NavMenu } from "./NavMenu";
import { SiteLogo } from "./SiteLogo";
import { SiteTitle } from "./SiteTitle";
import { PostTitle } from "./Post/PostTitle";
import { ShadowText } from "./ShadowText";
import { Footer } from "./Footer";
import dayjs from "dayjs";

export const MainLayout: React.FC<{ post?: Post; children: ReactNode }> = ({
  post,
  children,
}) => {
  return (
    <Stack align="center">
      {post ? (
        <>
          <Group wrap="nowrap" align="flex-start" maw={1200}>
            <SiteLogo width={160} />
            <Box maw={1000} pt={32}>
              <PostTitle post={post} deg={90} />
            </Box>
          </Group>
          <ShadowText deg={90}>
            published on Ramiel&apos;s Creations on{" "}
            {dayjs(post.date).format("MMM DD, YYYY")}
          </ShadowText>
        </>
      ) : (
        <>
          <Center>
            <SiteLogo />
          </Center>
          <SiteTitle />
          <ShadowText>a website by Fabrizio Ruggeri</ShadowText>
        </>
      )}

      <Divider w="50%" />
      <NavMenu />
      <Divider w="50%" />
      <Box component="main">{children}</Box>
      <Footer />
    </Stack>
  );
};
