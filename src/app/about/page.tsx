"use client";
import { Anchor, Image, List, Stack, Text } from "@mantine/core";
import { MainLayout } from "../_components/MainLayout";
import classes from "../_components/Post/Post.module.css";

export default function AboutPage() {
  return (
    <MainLayout>
      <Stack>
        <Image
          src="https://res.cloudinary.com/ramiel/image/upload/v1475943064/fabri-avatar-3logic_qdrltq.jpg"
          alt="profile picture"
          className={classes.image}
          maw={200}
          style={{ alignSelf: "center" }}
        />

        <Text>
          Hey everyone!
          <br />
          I&apos;m <em>Fabrizio Ruggeri</em>, a web developer living and working
          in Europe.
          <br />
        </Text>
        <Text>Let&apos;s connect:</Text>
        <List>
          <List.Item>
            Github: <Anchor href="https://github.com/ramiel">ramiel</Anchor>
          </List.Item>
          <List.Item>
            Twitter:{" "}
            <Anchor href="https://twitter.com/ramiel1">@ramiel1</Anchor>
          </List.Item>
          <List.Item>
            LinkedIn:{" "}
            <Anchor href="https://www.linkedin.com/in/fabrizioruggeri/">
              profile
            </Anchor>
          </List.Item>
          <List.Item>
            My{" "}
            <Anchor
              href="http://curriculum.ramielcreations.com"
              target="_blank"
            >
              curriculum
            </Anchor>{" "}
            (outdated)
          </List.Item>
        </List>
      </Stack>
      <div
        dangerouslySetInnerHTML={{
          __html: `<iframe src="https://line-21.com/player/TPEZG8xo"   name="Line 21"   style="border:0px #ffffff none;min-height: 340px;"   scrolling="no"   frameborder="0"   marginheight="0"   marginwidth="0"   width="800"  height="450"  allow="autoplay; picture-in-picture"   allowfullscreen  webkitallowfullscreen  mozallowfullscreen  oallowfullscreen  msallowfullscreen></iframe>`,
        }}
      />
    </MainLayout>
  );
}
