import { Stack, Box } from "@mantine/core";
import { getAllPosts } from "./_utils/api";
import { PostList } from "./_components/PostList";
import { MainLayout } from "./_components/MainLayout";

export default async function Page() {
  const posts = await getAllPosts([
    "title",
    "date",
    "slug",
    "author",
    "cover",
    "excerpt",
    "draft",
  ]);
  return (
    <MainLayout>
      <Stack align="center">
        <PostList posts={posts} />
      </Stack>
    </MainLayout>
  );
}
