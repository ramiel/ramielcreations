/**
 * Return a readable string of the date
 * @param date The date
 * @returns A readable date
 */
export const toReadableDate = (date: string) => {
  return new Date(date).toLocaleDateString('en-US', {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });
};
