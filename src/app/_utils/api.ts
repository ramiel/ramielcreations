import fs from "fs/promises";
import { join } from "path";
import matter from "gray-matter";
import { pick } from "radash";
import Post from "@/types/post";

const postsDirectory = join(process.cwd(), "_posts");

export function getPostFileNames() {
  return fs.readdir(postsDirectory);
}

export const getPostByFileName = async <TKeys extends keyof Post>(
  name: string,
  fields: TKeys[] = []
): Promise<Pick<Post, TKeys>> => {
  const fullPath = join(postsDirectory, name);
  const fileContents = await fs.readFile(fullPath, {
    encoding: "utf-8",
  });
  const { data, content } = matter(fileContents);

  const parsedPost = { ...data, content } as Post;

  return pick(parsedPost, fields);
};

export async function getAllPosts<TKeys extends keyof Post>(
  fields: TKeys[] = []
): Promise<Array<Pick<Post, TKeys | "date" | "draft">>> {
  const names = await getPostFileNames();
  const posts = await Promise.all(
    names.map(
      async (name) =>
        await getPostByFileName(name, [...fields, "date", "draft"])
    )
  );

  return posts // sort posts by date in descending order
    .filter((post) => !post.draft || !process.env.VERCEL)
    .sort((post1, post2) => (post1.date > post2.date ? -1 : 1));
}

export async function getPostBySlug<TKeys extends keyof Post>(
  slug: string,
  fields: TKeys[] = []
): Promise<undefined | Pick<Post, TKeys | "slug" | "date" | "draft">> {
  const posts = await getAllPosts([...fields, "slug"]);
  return posts.find((post) => post.slug === slug);
}
