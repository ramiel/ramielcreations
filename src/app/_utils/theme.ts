"use client";
import { Anchor, createTheme } from "@mantine/core";

export const theme = createTheme({
  fontFamily: "var(--font-main), serif",
  fontFamilyMonospace: "var(--font-mono), Monaco, Courier, monospace",
  colors: {
    ramiel: [
      "#edf4ff",
      "#dde5f3",
      "#bec8df",
      "#9aaac9",
      "#7d90b7",
      "#6a80ad",
      "#5f78a9",
      "#4f6694",
      "#445b87",
      "#374e79",
    ],
  },
  primaryColor: "ramiel",
  cursorType: "pointer",
  headings: {
    fontWeight: "600",
  },
  components: {
    Anchor: Anchor.extend({
      defaultProps: {
        underline: "hover",
      },
    }),
  },
});
