import { useComputedColorScheme, useMantineColorScheme } from "@mantine/core";
import { useCallback, useMemo } from "react";

const fakeAudio = {
  play: () => {},
} as HTMLAudioElement;

// cache to avoid loading same audio multiple times
const loadedAudio = new Map<string, HTMLAudioElement>();

function getAudioOrStub(path: string): HTMLAudioElement {
  if (typeof Audio !== "undefined" && typeof window !== "undefined") {
    if (!loadedAudio.has(path)) {
      loadedAudio.set(path, new Audio(path));
    }
    return loadedAudio.get(path) as HTMLAudioElement;
  }
  return fakeAudio;
}

/**
 * Color switcher for next
 */
export const useColoSwitcher = () => {
  const { setColorScheme } = useMantineColorScheme();
  const computedColorScheme = useComputedColorScheme(undefined, {
    getInitialValueInEffect: true,
  });
  const isDark = useMemo(
    () => computedColorScheme === "dark",
    [computedColorScheme]
  );

  const switchOnAudio = useMemo(
    () => getAudioOrStub("/audio/switchon.mp3"),
    []
  );
  const switchOffAudio = useMemo(
    () => getAudioOrStub("/audio/switchoff.mp3"),
    []
  );

  const toggleColorScheme = useCallback(() => {
    if (isDark) {
      switchOffAudio.play();
      setColorScheme("light");
    } else {
      switchOnAudio.play();
      setColorScheme("dark");
    }
  }, [isDark, setColorScheme, switchOffAudio, switchOnAudio]);

  return { toggleColorScheme, isDark };
};
