import { MetadataRoute } from "next";
import { getAllPosts } from "./_utils/api";

export default async function sitemap(): Promise<MetadataRoute.Sitemap> {
  const posts = await getAllPosts(["slug", "date"]);
  return [
    {
      url: "https://ramielcreations.com",
      lastModified: new Date(),
      changeFrequency: "yearly",
      priority: 1,
    },
    {
      url: "https://ramielcreations.com/about",
      lastModified: new Date(),
      changeFrequency: "yearly",
      priority: 0.5,
    },
    ...(posts || []).map((post) => ({
      url: `https://ramielcreations.com/${post.slug}`,
      lastModified: new Date(post.date),
      priority: 0.8,
    })),
  ];
}
