import type Author from "./author";

type ImageSpec = {
  src: string;
  width?: number;
  height?: number;
  alt?: string;
};

interface Post {
  title: string;
  date: string;
  slug: string;
  content: string;
  excerpt?: string;
  author?: Author;
  ogImage?: ImageSpec;
  cover?: ImageSpec;
  tags?: string[];
  draft?: boolean;
}

export default Post;
